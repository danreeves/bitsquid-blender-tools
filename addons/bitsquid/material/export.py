#    Bitsquid Blender Tools
#    Copyright (C) 2021  Lucas Schwiderski
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import os
import bpy
from bitsquid import step
from bpy_extras import io_utils


template = """
parent_material = "core/stingray_renderer/shader_import/standard"

textures = {}

variables = {
    base_color = {
        type = "vector3"
        value = [
            1
            0.333333333333333
            0
        ]
    }
    emissive = {
        type = "vector3"
        value = [
            0
            0
            0
        ]
    }
    emissive_intensity = {
        type = "scalar"
        value = 1
    }
    metallic = {
        type = "scalar"
        value = 0
    }
    roughness = {
        type = "scalar"
        value = 0.91
    }
    use_ao_map = {
        type = "scalar"
        value = 0
    }
    use_color_map = {
        type = "scalar"
        value = 0
    }
    use_emissive_map = {
        type = "scalar"
        value = 0
    }
    use_metallic_map = {
        type = "scalar"
        value = 0
    }
    use_normal_map = {
        type = "scalar"
        value = 0
    }
    use_roughness_map = {
        type = "scalar"
        value = 0
    }
}

"""


def build_filepath(context, material, mode='ABSOLUTE'):
    project_root = bpy.path.abspath(context.scene.bitsquid.project_root)
    base_src = os.path.dirname(context.blend_data.filepath)
    print(base_src, bpy.path.abspath(project_root))
    filename = "{}.material".format(material.name)
    return io_utils.path_reference(
        os.path.join(material.bitsquid.filepath, filename),
        base_src=base_src,
        base_dst=project_root,
        mode=mode,
    )


def save(self, context, material):
    filepath = build_filepath(context, material)
    self.report({'INFO'}, "Saving material to " + filepath)

    namespace = {
        'material': material,
    }
    content = step.Template(template, strip=False).expand(namespace)

    with open(filepath, "w", encoding="utf8", newline="\n") as f:
        f.write(content)

    return {'FINISHED'}
