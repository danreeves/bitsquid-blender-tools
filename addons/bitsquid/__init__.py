#    Bitsquid Blender Tools
#    Copyright (C) 2021  Lucas Schwiderski
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.


bl_info = {
    "name": "Bitsquid Engine",
    "author": "Lucas Schwiderski",
    "version": (0, 0, 1),
    "blender": (2, 90, 0),
    "location": "File > Import-Export",
    "description": "Import-Export Bitsquid related files",
    "category": "Import-Export",
}

# Reload sub modules if they are already loaded
if "bpy" in locals():
    import importlib
    if "unit_export" in locals():
        importlib.reload(unit_export)
    if "material_export" in locals():
        importlib.reload(material_export)


import bpy
from bpy.types import (
    Panel,
    Operator,
    PropertyGroup,
)
from bpy.props import (
    EnumProperty,
    BoolProperty,
    StringProperty,
    PointerProperty,
)
from bpy_extras.io_utils import (
    ExportHelper,
    axis_conversion,
    orientation_helper,
    path_reference_mode,
)
from bitsquid.unit import export as unit_export
from bitsquid.material import export as material_export


class BitsquidSettings(PropertyGroup):
    project_root: StringProperty(
        name="Project Root",
        description="The project directory considered as root path for all Bitsquid related operations.",
        default="//",
        subtype='DIR_PATH',
    )


class SCENE_PT_bitsquid(Panel):
    bl_label = "Bitsquid"
    bl_space_type = "PROPERTIES"
    bl_region_type = "WINDOW"
    bl_context = "scene"

    def draw(self, context):
        layout = self.layout
        layout.use_property_split = False
        layout.use_property_decorate = False

        bitsquid = context.scene.bitsquid
        layout.prop(bitsquid, "project_root", text="Project Root")


class BitsquidObjectSettings(PropertyGroup):
    filepath: StringProperty(
        name="Unit File Path",
        description="The directory to store the .unit file in. Needs to be within the project root.",
        default="//",
        subtype='DIR_PATH',
    )

    export_materials: BoolProperty(
        name="Export materials",
        description="Automatically export all referenced materials",
        default=True,
    )


class OBJECT_OT_bitsquid_export(Operator):
    """Export this object into a Bitsquid Unit file"""
    bl_idname = "object.bitsquid_export_unit"
    bl_label = "Export .unit"
    bl_options = {'REGISTER'}

    @classmethod
    def poll(cls, context):
        object = context.active_object
        return bpy.data.is_saved and object is not None and object.type == 'MESH'

    def execute(self, context):
        object = context.active_object
        if object.bitsquid.export_materials:
            for material_slot in object.material_slots.values():
                material_export.save(self, context, material_slot.material)

        return unit_export.save(self, context, object)


class OBJECT_PT_bitsquid(Panel):
    bl_label = "Bitsquid"
    bl_space_type = "PROPERTIES"
    bl_region_type = "WINDOW"
    bl_context = "object"

    def draw(self, context):
        object = context.object
        layout = self.layout

        if object.type != 'MESH':
            layout.label(text="Only objects of type 'MESH' are supported.")
            return

        layout.use_property_split = False
        layout.use_property_decorate = False

        bitsquid = object.bitsquid
        layout.prop(bitsquid, "filepath", text="Unit File Path")
        layout.prop(bitsquid, "export_materials", text="Export materials")
        layout.operator("object.bitsquid_export_unit", text="Export .unit")


class BitsquidMaterialSettings(PropertyGroup):
    filepath: StringProperty(
        name="Material File Path",
        description="The directory to store the .material file in. Needs to be within the project root.",
        default="//",
        subtype='DIR_PATH',
    )


class MATERIAL_OT_bitsquid_export(Operator):
    """Export this material into a Bitsquid Material file"""
    bl_idname = "object.bitsquid_export_material"
    bl_label = "Export .material"
    bl_options = {'REGISTER'}

    @classmethod
    def poll(cls, context):
        return bpy.data.is_saved and context.active_object is not None

    def execute(self, context):
        material = context.active_material
        return unit_export.save(self, context, material)


class MATERIAL_PT_bitsquid(Panel):
    bl_label = "Bitsquid"
    bl_space_type = "PROPERTIES"
    bl_region_type = "WINDOW"
    bl_context = "material"

    @classmethod
    def poll(cls, context):
        object = context.active_object
        return len(object.material_slots) > 0

    def draw(self, context):
        layout = self.layout
        layout.use_property_split = False
        layout.use_property_decorate = False

        bitsquid = context.material.bitsquid
        layout.prop(bitsquid, "filepath", text="Material File Path")
        layout.operator("object.bitsquid_export_material", text="Export .material")


# Register
classes = [
    BitsquidSettings,
    SCENE_PT_bitsquid,
    BitsquidObjectSettings,
    OBJECT_PT_bitsquid,
    OBJECT_OT_bitsquid_export,
    BitsquidMaterialSettings,
    MATERIAL_PT_bitsquid,
    MATERIAL_OT_bitsquid_export,
]


def register():
    from bpy.utils import register_class
    for cls in classes:
        register_class(cls)

    bpy.types.Scene.bitsquid = PointerProperty(type=BitsquidSettings)
    bpy.types.Object.bitsquid = PointerProperty(type=BitsquidObjectSettings)
    bpy.types.Material.bitsquid = PointerProperty(type=BitsquidMaterialSettings)


def unregister():
    del bpy.types.Scene.bitsquid

    from bpy.utils import unregister_class
    for cls in reversed(classes):
        unregister_class(cls)


if __name__ == "__main__":
    register()
